package view;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

public class NewImageController {
	
	@FXML
	ImageView imageViewConfirm;
	
	@FXML
	Button confirmButton;
	
	@FXML
	Button cancelButton;
	
	private ImageController parent;
	
	public NewImageController(){}
	
	@FXML
	public void initialize(){
		cancelButton.setOnAction(new EventHandler<ActionEvent>() {
			
			@Override
			public void handle(ActionEvent event) {
				if(getParent() != null){
					getParent().hidePromptNewImage();
				}
			}
		});
		confirmButton.setOnAction(new EventHandler<ActionEvent>() {
			
			@Override
			public void handle(ActionEvent event) {
				if(getParent() != null){
					getParent().loadImage(getImage());
					getParent().hidePromptNewImage();
				}
			}
		});
	}
	
	public void setImage(Image image){
		this.imageViewConfirm.setImage(image);
	}
	
	public Image getImage(){
		return this.imageViewConfirm.getImage();
	}

	public ImageController getParent() {
		return parent;
	}

	public void setParent(ImageController parent) {
		this.parent = parent;
	}

}
