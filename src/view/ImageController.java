package view;

import java.io.IOException;
import java.util.Observable;
import java.util.Observer;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import main.MainApp;
import model.ImageModel;
import util.Selector;

public class ImageController implements Observer{
	
	private ImageModel imageModel;
	private Selector selector;
	private Stage confirmStage;
	private NewImageController confirmStageController;
	
	@FXML
	private ImageView imageView;
	
	@FXML
	private Group group;
	
	public ImageController(){}
	
	@FXML
	public void initialize(){
		selector = new Selector(this.group);
		FXMLLoader loader = new FXMLLoader();
        loader.setLocation(MainApp.class.getResource("../view/NewImageConfirm.fxml"));
        AnchorPane confirmNewImage;
		try {
			confirmNewImage = (AnchorPane) loader.load();
			Scene secondScene = new Scene(confirmNewImage);
			
			confirmStage = new Stage();
			confirmStage.setResizable(false);
			confirmStage.setTitle("This is the new image preview. Do you want to use it?");
			confirmStage.setScene(secondScene);
			
			confirmStageController = loader.getController();
			confirmStageController.setParent(this);
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void loadImageModel(ImageModel imageModel){
		this.imageModel = imageModel;
	}
	
	public void loadImage(Image image){
		if(this.imageModel != null){
			this.imageModel.setImage(image);
		}
	}
	
	// FILTER FUNCTIONS
	
	public void crop(){
		if(imageModel != null){
			Image newImage = imageModel.crop(selector.getBounds());
			selector.clearSelector();
			showPromptNewImage(newImage);
		}
	}
	
	public void toGrayscale(){
		if(imageModel != null){
			Image newImage = imageModel.toGrayscale();
			showPromptNewImage(newImage);
		}
	}

	@Override
	public void update(Observable obs, Object obj) {
		if(obs == this.imageModel){
			this.imageView.setImage(this.imageModel.getImage());
		}
	}
	
	public void showPromptNewImage(Image image){
		if(confirmStage != null && confirmStageController != null){
			confirmStageController.setImage(image);
			confirmStage.show();
		}
	}
	
	public void hidePromptNewImage(){
		confirmStage.hide();
	}
	
}
