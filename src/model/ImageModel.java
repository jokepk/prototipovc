package model;

import javafx.geometry.Bounds;
import javafx.scene.image.Image;
import javafx.scene.image.PixelReader;
import javafx.scene.image.PixelWriter;
import javafx.scene.image.WritableImage;
import javafx.scene.paint.Color;

import java.util.Observable;

public class ImageModel extends Observable{

	private Image image;

	public ImageModel(){
		this.image = null;
	}
	public ImageModel(Image image){
		setImage(image);
	}

	public Image getImage(){
		return image;
	}

	public void setImage(Image image){
		this.image = image;
		setChanged();
		notifyObservers();
	}
	
	public int getPixelValue(int x, int y){
		return (int) this.image.getPixelReader().getColor(x, y).getRed() * 255;
	}

	// FILTERS

	public Image crop(Bounds bounds){

		PixelReader pixels = this.image.getPixelReader();

		WritableImage newImage = new WritableImage(pixels, (int) bounds.getMinX(), (int) bounds.getMinY(), 
				(int) bounds.getWidth(), (int) bounds.getHeight());

		return newImage;
	}

	public Image toGrayscale(){
		// Reader for the original image
		PixelReader pixelReader = this.image.getPixelReader();
		int width = (int) this.image.getWidth();
		int height = (int) this.image.getHeight();

		// Create new image and get its writer
		WritableImage result = new WritableImage(width, height);
		PixelWriter pixelWriter = result.getPixelWriter();

		// Apply transformation
		double newVal;
		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {
				// Actual color
				Color color = pixelReader.getColor(x, y);
				// Weighted sums according to human perception
				newVal = 0.299 * color.getRed() + 0.587 * color.getGreen()
				+ 0.114 * color.getBlue();
				// New color
				Color newColor = Color.color(newVal, newVal, newVal,
						color.getOpacity());
				// Write new pixel
				pixelWriter.setColor(x, y, newColor);
			}
		}

		return result;
	}
}
