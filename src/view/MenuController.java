package view;

import java.io.File;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.MenuItem;
import javafx.scene.image.Image;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import main.MainApp;
import model.ImageModel;

public class MenuController {

	@FXML
	private MenuItem openFile;
	
	@FXML
	private MenuItem cropButton;
	
	@FXML
	private MenuItem toGrayscale;

	private MainApp mainApp;
	private ImageController imageController;
	
	private ImageModel image;

	public MenuController(){
		this.image = null;
	}

	@FXML
	public void initialize(){

		openFile.setOnAction(new EventHandler<ActionEvent>() {
			
			@Override
			public void handle(ActionEvent event) {
				FileChooser fc = new FileChooser();
				
				fc.getExtensionFilters().add(new ExtensionFilter("Image Files (*.bmp)", "*.bmp"));
				
				File imageFile = fc.showOpenDialog(mainApp.getPrimaryStage());
				
				if(imageFile != null){
					
					Image imageLoaded = new Image(imageFile.toURI().toString());
					
					setImage(imageLoaded);
				}
				
			}
		});
		
		cropButton.setOnAction(new EventHandler<ActionEvent>() {
			
			@Override
			public void handle(ActionEvent event) {
				imageController.crop();
			}
		});
		
		toGrayscale.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				imageController.toGrayscale();
			}
		});

	}

	public void setMainApp(MainApp app){
		this.mainApp = app;
	}

	public void setImageController(ImageController ic){
		this.imageController = ic;
	}
	
	public void setImage(Image image){
		this.image = new ImageModel();
		this.imageController.loadImageModel(this.image);
		this.image.addObserver(this.imageController);
		this.image.setImage(image);
	}
}
